
<!-- README.md is generated from README.Rmd. Please edit that file -->

# fluentDashboard

<!-- badges: start -->
<!-- badges: end -->

A shiny-Dashboard using [Fluent
UI](https://developer.microsoft.com/en-us/fluentui)

This package based on components of the
[shiny.fluent](https://github.com/Appsilon/shiny.fluent) package and the
[dashboard-example](https://appsilon.github.io/shiny.fluent/articles/st-sales-reps-dashboard).

## Installation

### Dependencies

``` r
remotes::install_github("Appsilon/shiny.react")
remotes::install_github("Appsilon/shiny.fluent")
install.packages("shiny.router")
```

### Installation of fluentDashboard

``` r
remotes::install_gitlab("illoRocks/fluentDashboard")
```

## Dashboard-Example

``` r
library(fluentDashboard)
library(shiny)
library(shiny.router)

## ---- pages ----
home_page <- Page(
  title = "Home",
  subtitle = "First Part",
  Card(title = "Table", plotOutput("plot"))
)

analysis_page <- Page(
  title = "Analysis",
  subtitle = "Second Part"
)

## ---- router ----
router <- make_router(
  route("/", home_page),
  route("analysis", analysis_page)
)

## ---- sidebar ----
navigation <- Sidebar(
  list(name = "Home", url = "#!/", key = "home", icon = "Home"),
  list(name = "Analysis", url = "#!/analysis", key = "analysis", icon = "AnalyticsReport")
)

## ---- header ----
header <- FluentHeader(
  title = "Report",
  icon = "ViewDashboard"
)

## ---- shiny-app ----
shinyApp(
  ui = FluentDashboard(
    header = header,
    mainUI = router$ui,
    navigation = navigation
  ),
  server = function(input, output, session) {
    router$server(input, output, session)

    output$plot <- renderPlot({
      plot(cars)
    })
  }
)
```
