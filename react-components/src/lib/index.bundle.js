// @ts-ignore
const {
    InputAdapter
} = require('@/shiny.react')
const {
    Autocomplete
} = require('./autocomplete/autocomplete');
const {
    initializeIcons
} = require('@fluentui/react/lib/Icons')

initializeIcons(undefined, {
    disableWarnings: true
});

const AutocompleteIa = InputAdapter(Autocomplete, (value, setValue) => ({
    items: value,
    onSearch: (searchText) =>
        setValue({
            value: searchText,
            action: "search"
        }),
    onSelect: (searchText) =>
        setValue({
            value: searchText,
            action: "select"
        }),
    onChange: (searchText) =>
        setValue({
            value: searchText,
            action: "change"
        }),
}));


window.jsmodule = {
    ...window.jsmodule,
    'react-autocomplete': {
        Autocomplete: AutocompleteIa
    }
};