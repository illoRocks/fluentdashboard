import { List, SearchBox } from '@fluentui/react';
import { IAutocomplete, IAutocompleteList, IItem } from './autocomplete.types';
import { ChangeEvent, useState, KeyboardEvent, ReactNode, useEffect } from 'react';
import { hasMatch, KeyCodes } from './autocomplete.utils';
import { classNames } from './autocomplet.styles';

export const Autocomplete = ({
  items,
  placeholder = 'Search',
  serversideFiltering = false,
  onSearch = (value: string) => {
    alert(value);
  },
  onSelect = (value: string) => {
    alert(value);
  }, 
  onChange = (value: string) => {
    alert(value);
  },
}: IAutocomplete) => {
  const [currentFocus, setCurrentFocus] = useState(-1)
  const [currentList, setCurrentList] = useState(
    serversideFiltering ? items : [] as IItem[])
  const [searchValue, setSearchValue] = useState("")

  useEffect(() => {
    if (serversideFiltering) {
      setCurrentList(items)
    }
    console.log("items in list:", items)
  }, [items, serversideFiltering])


  const oninput = (e: ChangeEvent<HTMLInputElement> | undefined, value: string | undefined) => {

    if (!value) {
      setSearchValue("")
      return
    }
    setCurrentFocus(-1);
    setSearchValue(value)
    onChange(value)

    if (serversideFiltering) {
      return;
    }

    // filter items
    const filteredItems = items
      .filter(item => hasMatch(item.searchValue, value))
    setCurrentList(filteredItems)
    console.log("filtered list: ", currentList)
  }

  const addActive = (items: IItem[], currentFocus: number): IItem[] => {

    if (currentFocus >= items.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = - 1;
    setCurrentFocus(currentFocus)

    const activatedItems = items.map((value, i) => {
      if (i === currentFocus) {
        return { ...value, active: true }
      }
      return { ...value, active: undefined }
    })
    console.log(`addActive(_, ${currentFocus})`, activatedItems)
    return activatedItems
  }

  const onkeydown = (e: KeyboardEvent) => {
    console.log(`onkeydown(${e.key})`)
    if (e.key === KeyCodes.ArrowDown && currentList.length) {
      setCurrentList((x: IItem[]) => addActive(x, currentFocus + 1))
      e.preventDefault()
    } else if (e.key === KeyCodes.ArrowUp && currentFocus >= 0) {
      setCurrentList((x: IItem[]) => addActive(x, currentFocus - 1))
      e.preventDefault()
    }
  }

  return (
    <div
      onKeyDown={onkeydown}
      className={classNames.autocomplete}
      onBlur={(e) => {
        setCurrentList([])
      }}
    >
      <SearchBox
        id={'SuggestionSearchBox'}
        placeholder={placeholder}
        value={searchValue}
        onSearch={value => {
          if (currentFocus > -1) {
            const item = currentList[currentFocus]
            onSelect(item.key)
            setSearchValue(item.displayValue)
            setCurrentFocus(-1)
          } else {
            onSearch(value.trim())
          }
          setCurrentList([])
        }}
        onClear={() => {
          onSearch("")
          setCurrentList([])
        }}
        showIcon={true}
        disableAnimation={true}
        onChange={oninput}
        autoComplete="off"
        clearButtonProps={{ 'data-is-focusable': false } as any}
      />
      <AutocompleteList
        items={currentList}
        onClick={(item) => {
          console.log(item)
          onSelect(item.key)
          setSearchValue(item.displayValue)
          setCurrentList([])
        }}
      />
    </div>
  )
}



const AutocompleteList = ({ items, onClick }: IAutocompleteList) => {
  useEffect(() => {
    console.log("items in list:", items)
  }, [items])

  const onRenderCell = (item: IItem, index: number): ReactNode => {
    return (
      <div className={item.active ? classNames.itemCellActive : classNames.itemCell}
        onMouseDown={() => {
          console.log("clicked")
          onClick(item)}
        }
      >
        <span className={classNames.itemName}>
          {item.displayValue}
        </span>
      </div>
    );
  };

  return (
    <List id='SearchList'
      className={classNames.list}
      items={items}
      onRenderCell={onRenderCell as any}
    />
  )
}