import { getFocusStyle, getTheme, IRawStyle, ITheme, mergeStyleSets } from "@fluentui/react";

const theme: ITheme = getTheme();
const { palette, fonts } = theme;

const itemCellStyle: IRawStyle  = {
    border: "1px solid transparent",
    cursor: "pointer",
    padding: "0px 8px",
    color: "rgb(50, 49, 48)",
    minHeight: "32px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    selectors: {
        '&:hover': { background: palette.neutralLight },
    },
}

export const classNames = mergeStyleSets({
    itemCell: [
        getFocusStyle(theme, { inset: -1 }),
        itemCellStyle
    ],
    itemCellActive: {
        background: palette.neutralLight,
        ...itemCellStyle
    },
    itemName: [
        fonts.mediumPlus,
        {
            display: "flex",
            alignItems: "center"
        }
    ],
    autocomplete: {
        position: "relative",
        display: "inline-block"
    },
    list: {
        border: "none !important",
        boxShadow: `rgb(0 0 0 / 13%) 0px 3.2px 7.2px 0px,
                rgb(0 0 0 / 11%) 0px 0.6px 1.8px 0px`,
        position: "absolute",
        zIndex: "99",
        top: "100%",
        left: "0",
        right: "0",
        background: palette.white, 
    }
});
