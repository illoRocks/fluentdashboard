export interface IItem {
  key: string
  displayValue: string
  searchValue?: string
  active?: boolean
}

export interface IAutocomplete {
  items: IItem[]
  placeholder: string
  onSearch: (x: string) => void
  onChange: (x: string) => void
  onSelect: (x: string) => void
  serversideFiltering?: boolean
}

export interface IAutocompleteList {
  items: IItem[]
  onClick: (value: IItem) => void
}
