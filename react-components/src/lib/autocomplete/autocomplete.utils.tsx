export const hasMatch = (x: string | undefined, y: string, ignoreCase: boolean = true) => {
    if (!x) {
        throw new Error("searchValue not in item");
    }
    if (ignoreCase) {
        x = x.toLowerCase()
        y = y.toLowerCase()
    }
    return x.indexOf(y) >= 0
}

export enum KeyCodes {
    ArrowDown = "ArrowDown",
    ArrowUp = "ArrowUp",
    Enter = "Enter",
}