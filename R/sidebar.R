#' Sidebar
#'
#' @param ... Multiple lists of links. Example:
#' list(name = "Home", url = "#!/", key = "home", icon = "Home")
#'
#' @importFrom shiny.fluent Nav
#'
#' @export
Sidebar <- function(...) {
  links <- list(...)

  Nav(
    groups = list(
      list(links = links)
    ),
    initialSelectedKey = links[[1]]$key,
    styles = list(
      root = list(
        height = "100%",
        boxSizing = "border-box",
        overflowY = "auto"
      )
    )
  )
}
