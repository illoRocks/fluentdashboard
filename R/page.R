#' Page
#'
#' @param ... Content
#' @param title Page-Title
#' @param subtitle Subtitle
#' @param standalone If this is a standalone page directly pass
#' to \link[shiny]{shinyApp} then use `TRUE`
#'
#' @importFrom shiny tagList div span
#' @importFrom shiny.fluent fluentPage
#'
#' @example inst/examples/Page.R
#'
#' @export
Page <- function(..., title = NULL, subtitle = NULL, standalone = FALSE) {
  tl <- tagList(
    div(
      class = if (standalone) "fd-page fd-standalone" else "fd-page",
      div(
        class = "page-title",
        title %then% span(title,
          class = "ms-fontSize-32 ms-fontWeight-semibold", style =
            "color: #323130"
        ),
        subtitle %then% span(subtitle,
          class = "ms-fontSize-14 ms-fontWeight-regular", style =
            "color: #605E5C; margin: 14px;"
        )
      ),
      ...
    )
  )

  if (standalone) {
    return(
      fluentPage(
        use_fd_deps(),
        tl
      )
    )
  }

  tl
}
