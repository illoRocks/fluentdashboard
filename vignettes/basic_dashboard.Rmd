---
title: "Tutorial: Basic Dashboard"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Tutorial: Basic Dashboard}
  %\VignetteEncoding{UTF-8}
  %\VignetteEngine{knitr::rmarkdown}
---

```{r setup-knitr, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup-chunks, include=FALSE, cache=FALSE}
do.call(knitr::read_chunk, list(path = "../inst/examples/demo/basic-dashboard.R"))
```

```{r libraries, echo=FALSE, message=FALSE}
```

In this tutorial we will build a simple dashboard using `fluentDashboard`.

## Requirements

At the beginning all the necessary packages have to be loaded.

```r
library(fluentDashboard)
library(shiny)
library(shiny.fluent)
library(shiny.router)
library(shinipsum)
```

## Pages

In the next step we create two pages with a heading and a card element. A plot is rendered on one side and text is rendered on the other.

```{r pages}
```

## Sidebar

In the next step we create two pages with a heading and a card element. A plot is rendered on one side and text is rendered on the other.

```{r sidebar}
```

A link leads to the homepage. The other for analysis.

## Header

We provide the header with a `CommandBar` from shiny.fluent.

```{r header}
```

## Router

To navigate the different pages we use `make_router` from shiny.router.

```{r router}
```

## Shiny App

We provide the app with the created elements and add server functions for rendering the text and plot elements.

```{r shiny-app, eval=FALSE}
```



