if (interactive()) {
  ## ---- libraries ----
  library(fluentDashboard)
  library(shiny)

  items <- list(
    list(key="yellow", displayValue = "Bananas", searchValue = "yellow Bananas"),
    list(key="red", displayValue = "Apples", searchValue = "red Apples"),
    list(key="blue", displayValue = "Blueberries", searchValue = "blue Blueberries"),
    list(key="green", displayValue = "Grapes", searchValue = "green Grapes"),
    list(key="orange", displayValue = "Oranges", searchValue = "orange Oranges")
  )

  ## ---- shinyApp ----
  shinyApp(
    ui = Page(
      title = "Search-Page",
      standalone = TRUE,
      Card(
        title = "Search",
        Grid(
          Col(
            Autocomplete(
              inputId = "search",
              items = items,
              placeholder = "Search"
            )
          ),
          Col(
            div("Search result: ", verbatimTextOutput("search_result"))
          )
        )
      ),
      Card(
        title = "Reactive Search",
        Autocomplete(
          inputId = "reactive_search",
          items = list(),
          placeholder = "Search",
          serversideFiltering = TRUE
        )
      )
    ),
    server = function(input, output) {
      output$search_result <- renderPrint(input$search)

      search_value <- reactive({
        if (hasName(input$reactive_search, "action")) {
          return(input$reactive_search$value)
        }
      })

      # Custom logic for autocomplete
      observeEvent(input$reactive_search, {

        value <- input$reactive_search$value
        action <- input$reactive_search$action

        if (nchar(value) < 2) {
          updateAutocomplete(
            inputId = "reactive_search",
            items = list()
          )
          return(NULL)
        }

        search_func <- function(x="cal") {
          items_filtered <- list()
          for (i in items) {
            if (grepl(x,
                      i$searchValue,
                      ignore.case = TRUE)) {
              items_filtered <- c(
                items_filtered,
                list(list(key=i$key, displayValue=i$displayValue))
              )
            }
          }
          items_filtered
        }

        if (action == "change") {
          updateAutocomplete(
            inputId = "reactive_search",
            items = search_func(value)
          )
        }
      })
    }
  )
}
